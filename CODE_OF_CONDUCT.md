# Code of Conduct

All documentation, code and communication under this repository are covered by the [OLKi Code of Conduct](https://framagit.org/synalp/olki/olki/blob/develop/CODE_OF_CONDUCT.md).
