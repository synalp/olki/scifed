<!--
HACKING THIS DOCUMENT

This document uses W3C's reSpec.js. To understand its possibilities and how to
use it, see https://github.com/w3c/respec/wiki/ReSpec-Editor's-Guide
-->

<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>SciFed</title>
    <script src='https://www.w3.org/Tools/respec/respec-w3c' async class='remove'></script>
    <script class='remove'>
      var respecConfig = {
        specStatus: "unofficial", // see https://github.com/w3c/respec/wiki/specStatus
        publishDate: "2019-09-22",
        // noRecTrack: true,

        /* persons involved */
        editors: [{
          name: "Pierre-Antoine Rault",
          url: "https://rigelk.eu",
          company: "LORIA - Université de Lorraine",
          companyURL: "http://www.loria.fr/en/",
          w3cid: 115395
        }],
        authors: [{
          name: "Pierre-Antoine Rault",
          url: "https://rigelk.eu",
          company: "LORIA - Université de Lorraine",
          companyURL: "http://www.loria.fr/en/",
          w3cid: 115395
        },{
          name: "Christophe Cerisara",
          url: "https://members.loria.fr/CCerisara/index.html",
          company: "LORIA - CNRS",
          companyURL: "http://www.loria.fr/en/"
        }],
        wgURI: "https://olki.loria.fr/platform/group-sessions.html",
        canonicalURI: "https://synalp.frama.io/olki/scifed/",
        otherLinks: [{
          key: 'Repository',
          data: [{
            value: 'Framagit (Gitlab by Framasoft) synapl/olki/scifed',
            href: 'https://framagit.org/synalp/olki/scifed'
          }, {
            value: 'File a bug',
            href: 'https://framagit.org/synalp/olki/scifed/issues'
          }, {
            value: 'Commit history',
            href: 'https://framagit.org/synalp/olki/scifed/commits'
          }]
        }],
        shortName: "scifed",
        additionalCopyrightHolders: "This work was supported by the french PIA project “Lorraine Université d’Excellence”, reference ANR-15-IDEX-04-LUE, as part of the Open Language and Knowledge for Citizen (OLKi) project, and developed by Université de Lorraine and its laboratories LORIA, ATILF, IECL, Archives Henri Poincaré and CREM.",
        copyrightStart: 2019,

        /* local references (not W3C specs) */
        localBiblio: {
          "OAI-PMH": {
            title: "Protocol for Metadata Harvesting (OAI-PMH)",
            href: "http://www.openarchives.org/OAI/openarchivesprotocol.html",
            status: "REC",
            publisher: "Open Archives Initiative",
          },
          "OAI-ORE": {
            authors: [
              'Herbert Van de Sompel',
              'Carl Lagoze'
            ],
            title: "Object Reuse and Exchange (OAI-ORE)",
            href: "http://www.openarchives.org/ore/1.0/datamodel",
            status: "Beta Specification",
            publisher: "Open Archives Initiative",
          },
          "PAV": {
            authors: [
              'Paolo Ciccarese',
              'Stian Soiland-Reyes',
              'Khalid Belhajjame',
              'Alasdair JG Gray',
              'Carole Goble',
              'Tim Clark'
            ],
            title: "PAV ontology: Provenance, Authoring and Versioning",
            href: "http://dx.doi.org/10.1186/2041-1480-4-37",
            status: "REC",
            publisher: "Journal of Biomedical Semantics"
          }
        },

        /* Branding */
        logos: [{
          src: 'logo_loria.jpg',
          href: "http://www.loria.fr/",
          alt: "LORIA",
          width: 200,
          height: 112,
          id: 'loria-logo',
        }],

        /* reSpec-specific paramters */
        format: "markdown" // allows to use Markdown within the document for ease of use.
      };
    </script>
  </head>
  <body>
    <section id='abstract'>
      This specification describes SciFed, a standard for federation of scientific activities and content 
      using ActivityPub. It is intended to be used in the context of the [[[ActivityStreams-Vocabulary]]]
      and provides a vocabulary for activity types needed in the context of scientific exchanges.
      
      The vocabulary is meant to be used in the context of [[ActivityPub]] exchanges, of which we make
      dialectal recommendations that merge [[[ActivityStreams-Vocabulary]]] with `schema.org` vocabulary.

      <section class="informative">
        ### Author's Note

        This draft is heavily influenced by the [[ActivityStreams-Vocabulary]], and uses part of `schema.org`
        as its object types. The author is very thankful for their significant contributions and gladly stands on
        their shoulders.
      </section>
    </section>
    <section id='sotd'>
        The provided namespace is not deployed (see <a href="#namespaces">Namespaces</a>).
    </section>

    <section>
      # Introduction

      The SciFed Vocabulary relies on [[[ActivityStreams-Vocabulary]]] and extends it with a set of
      abstract properties that describe past, present and future Activities. The standard is meant as
      a modern replacement for [[OAI-PMH]], and provides functional parity with it.

      All SciFed implementations are expected to implement support for the Core Types.

      The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT",
      "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [[RFC2119]].

      This specification uses the common term URI to mean both IRI [[RFC3987]] and URI [[RFC3986]].
      JSON-LD natively supports IRIs without any special measures.

      <section>
        ## Namespaces

        <aside class="issue">
          Since the `scifed` namespace is not deployed yet, we provide the full context <a href="scifed.jsonld">here</a> instead.
          Please replace any further mention of the base URI with it from here onwards.
        </aside>

        SciFed makes use of part of the following namespaces:

        <dl>
          <dt><code>scifed</code>:</dt> (default namespace used as base URI)
          <dd><code>https://www.w3.org/ns/scifed#</code></dd>
          <dt><code>@vocab</code>:</dt>
          <dd><code>https://www.w3.org/ns/activitystreams#</code></dd>
          <dt><code>oai</code>:</dt>
          <dd><code>http://www.openarchives.org/OAI/2.0/</code></dd>
          <dt><code>pav</code>:</dt>
          <dd><code>http://purl.org/pav/</code></dd>
          <dt><code>schema</code>:</dt>
          <dd><code>http://schema.org/</code></dd>
          <dt><code>xsd</code>:</dt>
          <dd><code>http://www.w3.org/2001/XMLSchema</code></dd>
          <dt><code>http</code></dt>
          <dd><code>http://www.w3.org/2011/http#</code></dd>
        </dl>
      </section>

      <section>
        ## Conventions

        The examples included in this document use the normative JSON serialization defined by the 
        <a data-cite="!ActivityStreams-Core#syntaxconventions">ActivityStreams Core</a> specification.
      </section>

    </section>

    <section>
      # Core Types

      The Core Types provide the minimum requirement to replicate the basic functionalities of a data
      repository, relying solely on ActivityPub. They are meant as replacement for <a data-cite="!OAI-PMH#Item">OAI-PMH's Item</a>
      and <a data-cite="!OAI-PMH#Record">Record</a> notions.

      Base URI: `https://www.w3.org/ns/scifed#`.

      The SciFed Core Types include <a data-cite="!ActivityStreams-Vocabulary#dfn-activity">Object</a> subtypes that are mapped to common scientific applications:

<ul>
  <li><a href="#activity-types" class="internalDFN">Activity Types</a></li>
  <li><a href="#object-types" class="internalDFN">Object Types</a></li>
</ul>

      Implementations are expected to use properties that make sense within the 
      specific context and requirements of their applications. They MUST however avoid using extension 
      types or properties that unduly overlap with or duplicate the vocabulary defined here
      or in the [[[ActivityStreams-Vocabulary]]] that this document already extends.

      <section>
        ## Activity Types

        All Activity Types inherit the properties of the base Activity type. Some specific
        Activity Types are subtypes or specializations of more generalized Activity Types

        The Activity Types include:

<ul>
  <li>
    <a href="#dfn-read" class="internalDFN" data-link-type="dfn">Read</a>
  </li>
</ul>

        <table>
          <thead>
            <tr>
              <th>Class</th>
              <th colspan="2" style="width: 40%">Description</th>
              <th style="width: 40%">Properties</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-review">Read</dfn></td>
              <td style="width: 10%">URI:</td>
              <td><code>https://www.w3.org/ns/scifed#Read</code></td>
              <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "type": "Read",
      "attributedTo": [
        "https://example.com/actor/jane", // actors part of the review
      ],
      "object": "http://example.com/corpus/0001",
      "review": {
        "type": "schema:Rating",
        "schema:ratingValue": 3,
        "schema:worstRating": 1,
        "schema:bestRating": 5
      }
    }
  </pre>
</aside>
              </td>
            </tr>
            <tr>
              <td>Notes:</td>
              <td>
                <p>
                  Modification of the `Read` type to add review capabilities of part or all
                  of an Object. Typically a <a href="#dfn-corpus" class="internalDFN" data-link-type="dfn">Corpus</a> object.
                </p>
              </td>
            </tr>
            <tr>
              <td>Extends:</td>
              <td>
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-read" data-link-type="dfn">Read</a></code>
              </td>
            </tr>
            <tr>
              <td>Properties:</td>
              <td>
                <p>
                  <code><a href="#dfn-review" class="internalDFN" data-link-type="dfn">review</a></code>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </section>

      <section>
        ## Object and Link types

        All Object Types inherit the properties of the base <a data-cite="!ActivityStreams-Vocabulary#dfn-object">Object</a> 
        and <code><a href="https://schema.org/Review">schema:CreativeWork</a></code> types.

        Non-dataset documents should extend the `schema:CreativeWork` of which `schema:Dataset` is a subclass.

        The Object Types include:

        <ul>
          <li>
            <a href="#dfn-corpus" class="internalDFN" data-link-type="dfn">Corpus</a>
          </li>
        </ul>

        The Link Types include:

        <ul>
          <li>
            <a href="#dfn-metadata" class="internalDFN" data-link-type="dfn">Metadata</a>
          </li>
          <li>
            <a href="#dfn-wikidata" class="internalDFN" data-link-type="dfn">WikiData</a>
          </li>
        </ul>

        <table>
          <thead>
            <tr>
              <th>Class</th>
              <th colspan="2" style="width: 40%">Description</th>
              <th style="width: 40%">Properties</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-corpus">Corpus</dfn></td>
              <td style="width: 10%">URI:</td>
              <td><code>https://www.w3.org/ns/scifed#Corpus</code></td>
              <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "type": "Corpus",
      "id": "http://example.com/corpus/0001",
      "name": "Débats parlementaires sur l'Europe à l'Assemblée nationale (2002-2012)",
      "summary": "Une analyse contrastive sur corpus de débats parlementaires français, allemands et britanniques",
      "published": "2019-05-31T00:00:00.000",
      "inLanguage": "fr",
      "aboutLanguage": ["fr", "de", "en"],
      "originallyPublishedAt": "2019-01-31T00:00:00.000",
      "attachment": [
        "http://example.com/document-1",
        {
          "type": "Image",
          "content": "This is what he looks like.",
          "url": "http://example.org/cat.jpeg"
        },
        {
          "id": "http://example.com/blog/0001",
          "type": "Article",
          "prov:wasDerivedFrom": {
              "@id": "http://example.com/document-2"
          },
          "pav:authoredBy": {
              "@id": "http://orcid.org/0000-0001-9842-9718",
              "foaf:name": "Stian Soiland-Reyes"
          }
        }
      ],
      "url": [
        {
          "href": "http://example.com/corpus/0001#oai_dc",
          "mediaType": "application/xml",
          "type": ["Link", "Metadata"],
          "oai:metadataPrefix": "oai_dc",
          "oai:metadataNamespace": "http://www.openarchives.org/OAI/2.0/oai_dc/",
          "oai:schema": "http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
          "http:RequestHeader": {
            "http:fieldValue": "application/xml",
            "http:fieldName": "Accept"
          }
        }
      ],
      "similarTo": "http://other.example.com/document-12345"
    }
  </pre>
</aside>
              </td>
            </tr>
            <tr>
              <td>Notes:</td>
              <td>
                <p>
                  An ensemble of files or links, usually described with additional metadata.
                </p>
              </td>
            </tr>
            <tr>
              <td>Extends:</td>
              <td>
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code>
                |
                <code><a href="https://schema.org/Dataset">schema:Dataset</a></code>.
              </td>
            </tr>
            <tr>
              <td>Properties:</td>
              <td>
                <p>
                  <code><a href="#dfn-inLanguage" class="internalDFN" data-link-type="dfn">inLanguage</a></code>
                  |
                  <code><a href="#dfn-aboutLanguage" class="internalDFN" data-link-type="dfn">aboutLanguage</a></code>
                  |
                  <code><a href="#dfn-audiences" class="internalDFN" data-link-type="dfn">audiences</a></code>
                </p>
                <p>
                  Inherits all properties from
                  <code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code>
                  and
                  <code><a href="https://schema.org/Dataset">schema:Dataset</a></code>.
                </p>
                <p>
                  Extends what <code><a data-cite="!ActivityStreams-Vocabulary#dfn-attachment" data-link-type="dfn">attachment</a></code>
                  can express. See <code><a href="#dfn-attachment" class="internalDFN" data-link-type="dfn">attachment</a></code>.
                </p>
              </td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-metadata">Metadata</dfn></td>
              <td style="width: 10%">URI:</td>
              <td><code>https://www.w3.org/ns/scifed#Metadata</code></td>
              <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "href": "http://example.com/corpus/0001#oai_dc",
      "mediaType": "application/xml",
      "type": ["Link", "Metadata"],
      "prov:alternateOf": "http://example.com/corpus/0001",
      "oai:metadataPrefix": "oai_dc",
      "oai:metadataNamespace": "http://www.openarchives.org/OAI/2.0/oai_dc/",
      "oai:schema": "http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
      "http:RequestHeader": {
        "http:fieldValue": "application/xml",
        "http:fieldName": "Accept"
      }
    }
  </pre>
</aside>
              </td>
            </tr>
            <tr>
              <td>Notes:</td>
              <td>
                <p>
                  A specialized <code><a data-cite="!ActivityStreams-Vocabulary#dfn-link" data-link-type="dfn">Link</a></code>
                  that represents an metadata for a document, usually used in a <a href="#dfn-corpus" class="internalDFN" data-link-type="dfn">Corpus</a>
                  object's attachment or url field.
                </p>
              </td>
            </tr>
            <tr>
              <td>Extends:</td>
              <td>
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-link" data-link-type="dfn">Link</a></code>
              </td>
            </tr>
            <tr>
              <td>Properties:</td>
              <td>
                Inherits all properties from
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-link" data-link-type="dfn">Link</a></code>,
                with the addition of 
                `prov:alternateOf` | `oai:metadataPrefix` | `oai:metadataNamespace` | `oai:schema` | `http:RequestHeader`
              </td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-wikidata">WikiData</dfn></td>
              <td style="width: 10%">URI:</td>
              <td><code>https://www.w3.org/ns/scifed#WikiData</code></td>
              <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "id": "https://wikidata.org/entity/Q336",
      "href": "http://example.com/tag/Q336",
      "type": ["Link", "WikiData"],
      "name": "Science",
    }
  </pre>
</aside>
              </td>
            </tr>
            <tr>
              <td>Notes:</td>
              <td>
                <p>
                  An alternative to <code><a data-cite="!ActivityStreams-Vocabulary#dfn-hashtag" data-link-type="dfn">Hashtag</a></code>
                  that represents a WikiData entry.
                </p>
              </td>
            </tr>
            <tr>
              <td>Extends:</td>
              <td>
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-link" data-link-type="dfn">Link</a></code>
              </td>
            </tr>
            <tr>
              <td>Properties:</td>
              <td>
                Inherits all properties from
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-link" data-link-type="dfn">Link</a></code>.
              </td>
            </tr>
          </tbody>
        </table>
      </section>
    </section>

    <section>
      # Properties

      Base URI: `https://www.w3.org/ns/scifed#`.

      The "Domain" indicates the type of Object the property term applies to.
      The "Range" indicates the type of value the property term can have.
      Certain properties are marked as a "Subproperty Of" another term, meaning that the 
      term is a specialization of the referenced term.
      Properties marked as being "Functional" can have only one value. 
      Items not marked as "Functional" can have multiple values.

      <table>
        <thead>
          <tr>
            <th>Term</th>
            <th colspan="2" style="width: 40%">Description</th>
            <th style="width: 40%">Example</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-attachment">attachment</dfn></td>
            <td style="width: 10%">URI:</td>
            <td>`@attachment`</td>
            <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "type": "Corpus",
      "name": "Débats parlementaires sur l'Europe à l'Assemblée nationale (2002-2012)",
      "attachment": [
        { // reference files of another Corpus
          "id": "http://example.com/corpus/00001/files"
          "type": "Collection"
        },
        { // an OAI-ORE Aggregation
          "@id": "http://example.com/corpora/1",
          "@type": "ore:Aggregation",
          "ore:similarTo": "https://hdl.handle.net/11403/fr-parl/v1",
          "ore:aggregates": [
            "http://example.com/files/data-1",
            { "@id": "http://example.org/files/data-2",
              "@type": ["ore:AggregatedResource",
                "http://purl.org/dc/dcmitype/Dataset",
                "http://www.w3.org/ns/dcat#Dataset"],
              "pav:derivedFrom": {
                "@id": "http://example.com/files/data-1"
              },
              "pav:authoredBy": {
                "@id": "https://cv.archives-ouvertes.fr/naomi-truan",
                "foaf:name": "Naomi Truan"
              }
            }
          ]
        }
      ]
    }
  </pre>
</aside>
            </td>
          </tr>
          <tr>
            <td>Notes:</td>
            <td>
              A modified `attachment`, with potential `prov` and `pav` properties. Supports Collections and other Corpus as an attachment.
            </td>
          </tr>
          <tr>
            <td>Domain:</td>
            <td><code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code></td>
          </tr>
          <tr>
            <td>Range:</td>
            <td>
              <code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code>
            </td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-inLanguage">inLanguage</dfn></td>
            <td style="width: 10%">URI:</td>
            <td>`@inLanguage`</td>
            <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "type": "Corpus",
      "name": "Débats parlementaires sur l'Europe à l'Assemblée nationale (2002-2012)",
      "inLanguage": "en"
    }
  </pre>
</aside>
            </td>
          </tr>
          <tr>
            <td>Notes:</td>
            <td>
              Language in which the described resource is, represented by a [[RFC5646]]-controlled vocabulary.
            </td>
          </tr>
          <tr>
            <td>Domain:</td>
            <td><code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code></td>
          </tr>
          <tr>
            <td>Range:</td>
            <td>
              `xsd:string` | `rdf:langString`
            </td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-aboutLanguage">aboutLanguage</dfn></td>
            <td style="width: 10%">URI:</td>
            <td>`@aboutLanguage`</td>
            <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "type": "Corpus",
      "name": "Débats parlementaires sur l'Europe à l'Assemblée nationale (2002-2012)",
      "aboutLanguage": ["fr", "de", "en"]
    }
  </pre>
</aside>
            </td>
          </tr>
          <tr>
            <td>Notes:</td>
            <td>
              List of languages about which the described resource is, if different from `@inLanguage`, represented by a [[RFC5646]]-controlled vocabulary.
            </td>
          </tr>
          <tr>
            <td>Domain:</td>
            <td><code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code></td>
          </tr>
          <tr>
            <td>Range:</td>
            <td>
              @set of `xsd:string` | `rdf:langString`
            </td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td rowspan="4"><dfn data-dfn-type="dfn" id="dfn-audiences">audiences</dfn></td>
            <td style="width: 10%">URI:</td>
            <td>`@audiences`</td>
            <td rowspan="4">
<aside class="example">
  <pre>
    {
      "@context": "https://www.w3.org/ns/scifed#",
      "type": "Corpus",
      "name": "Débats parlementaires sur l'Europe à l'Assemblée nationale (2002-2012)",
      "audiences": [{
        "type": "schema:Audience",
        "audienceType": "scientific",
        "identifier": "Q336"
      }]
    }
  </pre>
</aside>
            </td>
          </tr>
          <tr>
            <td>Notes:</td>
            <td>
              List of domains/audiences the resource is targetted at, if any. Some platforms will want to see only scientific datasets for instance, and it would be superfluous to use the 'tag' property to describe the Dataset as 'scientific'.
            </td>
          </tr>
          <tr>
            <td>Domain:</td>
            <td>
              <p>
                <code><a data-cite="!ActivityStreams-Vocabulary#dfn-object" data-link-type="dfn">Object</a></code>
              </p>
            </td>
          </tr>
          <tr>
            <td>Range:</td>
            <td>
              <p>
                @set of <code><a href="https://schema.org/Audience">schema:Audience</a></code>.
              </p>
              <p>
                Intended `auidenceType` values are "scientific", "artistic", "politic", with their respective WikiData identifiers.
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </section>

    <section class="information">
      # Implementation Notes

      <section>
        ## Object purpose rather than object origin

        You can specify the general nature of the `schema:CreativeWork` via the
        <code><a href="#dfn-audiences" class="internalDFN" data-link-type="dfn">audiences</a></code> attribute, and
        are encouraged to do so, especially if the content is scientific. This
        allows to clearly state (a) domain(s) of interest, and those only interested
        by one (i.e.: institutional repositories and aggregators) to filter out the others
        without relying only on the origin of an object and thus favor *a-posteriori*
        moderation.
      </section>

      <section>
        ## Object Type Motivating Use Cases

        Corpora typically hold a lot of information about themselves and their content individual files.
        They are versatile containers for all type of data - keeping in mind they were designed with 
        scientists' needs as requirements - and can be used for non-scientific activities that require
        this versatility.

        Among the tackled problems, are those of existing metadata description held in OAI-PMH repositories.
        It is often vastly more descriptive than the base ActivityStreams Object that has not as many 
        descriptive attributes as, say, Dublin Core or CMDI. It has however,
        a `url` attribute holding information about the Object itself, whereas `attachment`
        holds files and potentially metadata about some of these files. This allows us to select `url` as
        the place where matadata currently shared by OAI-PMH implementations can be held. In that sense,
        no information is lost while transitioning to/complementing with an ActivitiyPub implementation.

        Hashtags and generally tags used in most institutional repositories suffer from different
        writings and the lack of internationalization. Using alternative tags relying on WikiData
        ids allows multiple representations depending on the selected language while keeping the
        semantic meaning and unicity of filtering.
      </section>
    </section>
  </body>

  <style>
    html {
      background: white !important; /* one 'unofficial draft' mention is enough :) */
    }
    table, td, th {
      border: 1px solid gray;
    }
    table {
      width: 100%;
      word-wrap: normal;
      overflow-wrap: normal;
      hyphens: manual;
    }
    th, td {
      text-align: left;
      text-align: start;
    }
    td {
      vertical-align: top;
      padding: 5px;
    }
  </style>
</html>
